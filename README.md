# Custom menu links for PeerTube

## Features
* Filter left menu links.
* Add left menu sections and links.

## Usage

1. Install plugin
1. Go to plugin settings
1. Enable plugin and add your sections.
